
// the "var target = $(event.target)" can is used several times, but it doesnt seem to work when I tried to make a a global var. I dont quite understand why that it though.

$('.region-nav').click(function () {
    $('.region-list').toggle('.open');
    $(document.body).mousedown(function(event) {
        var target = $(event.target); 
        if (!target.parents().andSelf().is('.region-nav')) { 
            $('.region-list').hide('.open');
        }
    });
    $(window).on('scroll', function(e){ 
        var scroll = $(window).scrollTop();
        if(scroll > 5){
            $('.region-list').hide('.open'); 
        }
    });
});


$('.hamburger').click(function () {
    $('.main-nav').toggle('.open');
    $(document.body).mousedown(function(event) {
        var target = $(event.target);
        if (!target.parents().andSelf().is('.hamburger')) { 
            $('.main-nav').hide('.open');
        }
    });

    $(window).on('scroll', function(e){ 
        var scroll = $(window).scrollTop();
        if(scroll > 5){
            $('.main-nav').hide('.open'); 
        }
    });
    
});



// SUB-PAGE INFORMATION 

// [Question: the following js is based on the global var below by countries. It seems to be working well, 
// but does it matter that the global var is below? I just thought that it looks neater this way, but not sure if it affects in any other way.]


var countrySelect = $('.country-active').text().toLowerCase().replace(/ /,'');

$('.country-active a').removeAttr('href');

$('.side-nav a').click(function(){

    var name = $(this).text().toLowerCase().replace(/ /,'');

    $('.country-active a').attr('href', countrySelect+'.html');

    $('.output').html(window[countrySelect+name]);

    $('.active').text($(this).text());

    $('.side-nav a').removeAttr('href').removeClass('side-active');

    $(this).addClass('side-active');

});



    // SINGAPORE

var singaporesocialcontribution = `                        
    <h1>Singapore - Social Contribution</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;

var singaporeincometax = `                        
    <h1>Singapore - Income Tax</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;

var singaporeadditionalinformation = `                        
    <h1>Singapore - Additional Information</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;


// JAPAN

var japansocialcontribution = `                        
    <h1>Japan - Social Contribution</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;

var japanincometax = `                        
    <h1>Japan - Income Tax</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;

var japanadditionalinformation = `                        
    <h1>Japan - Additional Information</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;



// CHINA

var chinasocialcontribution = `                        
    <h1>China - Social Contribution</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;

var chinaincometax = `                        
    <h1>China - Income Tax</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;

var chinaadditionalinformation = `                        
    <h1>China - Additional Information</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;


// United Kingdom

var unitedkingdomsocialcontribution = `                        
    <h1>United Kingdom - Social Contribution</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;

var unitedkingdomincometax = `                        
    <h1>United Kingdom - Income Tax</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;

var unitedkingdomadditionalinformation = `                        
    <h1>United Kingdom - Additional Information</h1>
                            
    <p>
        With a highly respected position in the global economy, 
        Singapore presents an exceptional opportunity for companies 
        seeking to extend their global operations. In addition to its 
        economic advantages, Singapore offers foreign companies high-levels 
        of security and safety, a business friendly environment, and 
        intellectual property protection. If you’re looking to make a 
        permanent move to Singapore, our legal specialists can also assist 
        you to establish a compliant foreign subsidiary. To find out more, 
        get in touch with our team – at our Singapore Office who are proficient 
        in Mandarin and English.
    </p>`
;


